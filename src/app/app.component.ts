import { Component } from '@angular/core';

@Component({
  selector: 'jump-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'jump';
}
