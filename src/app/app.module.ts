import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LibraryService } from './library.service';
import { LibraryDataService } from './library.data.service';

import { LibrarySearchComponent } from './library-search/library-search.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { BookListingComponent } from './book-listing/book-listing.component';
import { BookComponent } from './book/book.component';
import { BookDetailComponent } from './book-detail/book-detail.component';

const routes: Routes = [
  {
      path: 'browse',
      component: BookListingComponent,
  },
  {
      path: 'books/:id',
      component: BookDetailComponent,
  },
  {
    path: '',
    redirectTo: '/browse',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LibrarySearchComponent,
    NavBarComponent,
    BookListingComponent,
    BookComponent,
    BookDetailComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes,
    ),
    NgbModule.forRoot(),
    HttpClientModule,
    FormsModule,
  ],
  providers: [LibraryService, LibraryDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
