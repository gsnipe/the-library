import { Component, Input, OnInit } from '@angular/core';
import { LibraryService } from '../library.service';
import * as Data from '../app.interfaces';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'jump-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.sass']
})
export class BookDetailComponent implements OnInit {
  @Input() book: Data.Book | Data.Book[];
  id: Observable<string>;

  constructor(private libraryService: LibraryService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.pipe(map(p => p.id)).subscribe(results =>
      this.libraryService.getBookByID(results).subscribe(book => this.book = book)
    );
  }
}
