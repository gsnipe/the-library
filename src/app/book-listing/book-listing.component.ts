import { Component, HostBinding } from '@angular/core';
import { LibraryService } from '../library.service';

@Component({
  selector: 'jump-book-listing',
  templateUrl: './book-listing.component.html',
})

export class BookListingComponent {
  @HostBinding('attr.class') cssClass = 'library-listing-area col-md-9 row';
  books$;

  constructor(private libraryService: LibraryService) {
    this.books$ = this.libraryService.books$;
  }

}
