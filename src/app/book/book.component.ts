import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jump-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.sass']
})
export class BookComponent implements OnInit {
  @Input() book;

  constructor() { }

  ngOnInit() {
  }

}
