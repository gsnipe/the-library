import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SearchService } from './search.service';
import { LibraryService } from '../library.service';
import * as Data from '../app.interfaces';

@Component({
  selector: 'jump-library-search',
  templateUrl: './library-search.component.html',
  providers: [SearchService]
})

export class LibrarySearchComponent {
  results: Data.Results;
  searchTerm$ = new BehaviorSubject<string>('');

  constructor(private searchService: SearchService) {
    this.searchService.search(this.searchTerm$)
      .subscribe(results => {
          this.results = results;
      });
  }
}
