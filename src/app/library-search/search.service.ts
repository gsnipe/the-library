import { Injectable } from '@angular/core';
import { LibraryService } from '../library.service';
import { Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged, switchMap, filter } from 'rxjs/operators';
import * as Data from '../app.interfaces';


@Injectable()
export class SearchService {
  _books$: Observable<Array<Data.Book>>;

  constructor(private libraryService: LibraryService) {
    this._books$ = libraryService.books$;
  }

  search(terms$: Observable<string>) {
    return terms$.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(term => this.searchEntries(term))
    );
  }

  searchEntries(term) {
    return this._books$.pipe(
      map(books => term.length < 2 ? [] :
          books.filter(book =>
            (book.title.toLowerCase().indexOf(term.toLowerCase()) > -1 || book.author.toLowerCase().indexOf(term.toLowerCase())) > -1
      ))
    );

  }

}
