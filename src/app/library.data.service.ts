import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class LibraryDataService {
    constructor(private readonly http: HttpClient) {}

    getBooksData(): Observable<any> {
        return this.http.get('/assets/data/books.json');
    }
}
