import { Injectable } from '@angular/core';
import { LibraryDataService } from './library.data.service';
import { Observable } from 'rxjs';
import * as Data from './app.interfaces';
import { map, filter, take } from 'rxjs/operators';

@Injectable()
export class LibraryService {
    books$: Observable<Array<Data.Book>>;

    constructor(private libraryDataService: LibraryDataService) {
        this.books$ = this.getBooks();
    }

    getBooks(): Observable<any> {
        return this.libraryDataService.getBooksData();
    }

    getBookByID(id): Observable<Data.Book | Data.Book[]> {
        return this.books$.pipe(
            map(books => books.find(book =>
                  book['id'].toString() === id
            ))
        );
    }
}
